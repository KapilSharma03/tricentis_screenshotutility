# Tricentis_ScreenshotUtility

How to configure ScreenShot utility:
1. Copy the folder "ScreenshotUtility" in "C:\Tosca_Projects"

2. Trigger the file "PythonPackage.bat" as Administrator and wait till the command prompt closes

3. Trigger the file "DependentPackages.bat" as Administrator and wait till the command prompt closes

4. Copy the dll "ScreenShotCompareSET.dll" to "C:\Program Files (x86)\TRICENTIS\Tosca Testsuite\TBox" and restart Tosca

5. Import the sample subset to get access to the Screenshot custom module

6. Use the utility as per example TestCase

Note: Please make sure that all bat files and dll files are unblocked by right click on the files and checking properties

